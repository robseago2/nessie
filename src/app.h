
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#define MAX_CHANNELS 4 // ADC has 4 channes

typedef struct {
    int temperature_valid;
    int32_t temperature_raw;
    double temperature;

    double voltage;
    double turbidity;
    double conductivity;
    double ph;
}sensor_t;

extern sensor_t Sensor;

/* i2c.c */
extern void i2c_init(void); 
extern void i2c_release(void);
extern int8_t i2c_read(uint8_t address, uint8_t* data, uint16_t count);
extern int8_t i2c_write(uint8_t address, const uint8_t* data, uint16_t count);

/* ads1115.c */
extern void ads1115_init(void);
extern int ads1115_read(int chan);
extern double ads1115_convert_to_voltage(int val);

/* ds18b20.c */
extern int ds18b20_init(void);
extern void ds18b20_read(void);

/* gpio.c */
extern void gpio_export(int gid, const char *action);
extern void gpio_set_direction(int gid, const char *direction);
extern void gpio_set_value(int gid, const char *value);
extern int gpio_lookup(uint8_t channel);
extern void gpio_export_all(void);
extern void gpio_unexport_all(void);

/* calibrate.c */
extern double compute_ph(double voltage, double temperature);
extern double compute_turbidity(double voltage, double temperature);
extern double compute_conductivity(double voltage, double temperature);
