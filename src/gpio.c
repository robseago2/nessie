#include <sys/stat.h>
#include <fcntl.h>

#include "app.h"

static uint8_t channel_gpio[MAX_CHANNELS] = {0xff, 27, 23, 0xff};


// action can be 'export' or 'unexport'
void gpio_export(int gid, const char *action)
{
    char idstr[8];
    char fname[64];
    int fd;
    size_t slen;

    sprintf(fname, "/sys/class/gpio/%s", action);
    fd = open(fname, O_WRONLY);
    if (fd == -1) {
        printf("Unable to open %s\n", fname);
        return;
    }

    sprintf(idstr, "%d", gid);
    slen = strlen(idstr);
    if (write(fd, idstr, slen) != (int)slen) {
        printf("Error writing to %s\n", fname);
    }

    close(fd);
}

void gpio_set_direction(int gid, const char *direction)
{
    char fname[64];
    int fd;
    size_t slen;

    sprintf(fname, "/sys/class/gpio/gpio%d/direction", gid);
    fd = open(fname, O_WRONLY);
    if (fd == -1) {
        printf("Unable to open %s\n", fname);
        return;
    }
  
    slen = strlen(direction); 
    if (write(fd, direction, slen) != (int)slen) {
        printf("Error writing to %s\n", fname);
    }
    close(fd);
}


void gpio_set_value(int gid, const char *value)
{
    char fname[64];
    int fd;
    size_t slen;

    sprintf(fname, "/sys/class/gpio/gpio%d/value", gid);
    fd = open(fname, O_WRONLY);
    if (fd == -1) {
        printf("Unable to open %s\n", fname);
        return;
    }

    slen = strlen(value); 
    if (write(fd, value, slen) != (int)slen) {
        printf("Error writing to %s\n", fname);
    }
    close(fd);
}

int gpio_lookup(uint8_t channel)
{

    if(channel >= MAX_CHANNELS) {
        printf("channel %u is invalid\n", channel);
        return 0xff;
    }

    return (channel_gpio[channel]);
}

void gpio_export_all(void)
{
    for(int channel = 0; channel < MAX_CHANNELS; channel++) {
        uint8_t gid = gpio_lookup(channel);
        if(gid != 0xff) 
            gpio_export(gid, "export");
    }
}

void gpio_unexport_all(void)
{
    for(int channel = 0; channel < MAX_CHANNELS; channel++) {
        uint8_t gid = gpio_lookup(channel);
        if(gid != 0xff) 
            gpio_export(gid, "unexport");
    }
}

