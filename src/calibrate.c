#include <stdio.h>

#include "app.h"

#define MIN_PH_VOLTAGE 0.0
#define MAX_PH_VOLTAGE 3.3

// basic calibration
// ph=4 (V=2.030)
// ph=7 (V=1.505)

double compute_ph(double voltage, double temperature)
{
    double ph = 0.0;

    /* check bounds */
    if(voltage < MIN_PH_VOLTAGE) {
        voltage = MIN_PH_VOLTAGE;
        printf("Limit exceeded!\n");
    }

    else if (voltage > MAX_PH_VOLTAGE) {
        voltage = MAX_PH_VOLTAGE;
        printf("Limit exceeded!\n");
    }

    ph = 7.0 - 3.0*(voltage - 1.505)/0.525;

    return ph;
}

/* see https://forum.arduino.cc/t/getting-ntu-from-turbidity-sensor-on-3-3v/658067/9 */
/* 5V - coefficients need to change, no temp comp */
//#define MAX_TURBIDITY_VOLTAGE 4.2  // 0
//#define MIN_TURBIDITY_VOLTAGE 2.5  // 3000
// midpoint 3.35  // 2310
//#define KA (-1120.4)
//#define KB (5742.3)
//#define KC (-4352.9)

//#define MAX_TURBIDITY_VOLTAGE 2.772
//#define MIN_TURBIDITY_VOLTAGE 1.65
//#define KA (-2572.2)
//#define KB (8700.5)
//#define KC (-4352.9)

/* using LINEST in excel */

//#define MAX_TURBIDITY_VOLTAGE 2.200  // no impurities (0)
//#define MIN_TURBIDITY_VOLTAGE 1.310 // x0.524 (3000)
//#define MID_TURBIDITY_VOLTAGE ((MAX_TURBIDITY_VOLTAGE - MIN_TURBIDITY_VOLTAGE)/2.0) // 1.755V (2310)
//#define KA (-4090.392627)
//#define KB (10986.491605)
//#define KC (-4372.781214)

// calibration at 3.3V
#define MAX_TURBIDITY_VOLTAGE 2.230  // no impurities in water (0)
#define MIN_TURBIDITY_VOLTAGE 0.000  // covered with tape (3000?)
#define MAX_NTU 3000.0
#define KA (MAX_NTU/MAX_TURBIDITY_VOLTAGE)

double compute_turbidity(double voltage, double temperature)
{
    double turb = 0.0;

    /* check bounds */
    if(voltage < MIN_TURBIDITY_VOLTAGE) {
        voltage = MIN_TURBIDITY_VOLTAGE;
        printf("Limit exceeded!\n");
    }

    else if (voltage > MAX_TURBIDITY_VOLTAGE) {
        voltage = MAX_TURBIDITY_VOLTAGE;
        printf("Limit exceeded!\n");
    }

//    turb = KA*voltage*voltage + KB*voltage + KC;
    turb = MAX_NTU - KA*voltage;

    if (turb < 0.0)
        turb = 0.0;

    return turb;
}


// this equation was probably computed for 5.5V

#define MIN_CONDUCTIVITY_VOLTAGE 0.0
#define MAX_CONDUCTIVITY_VOLTAGE 2.3

double compute_conductivity(double voltage, double temperature)
{
    double cond = 0.0;
    double tc;
    double vc;

    /* check bounds */
    if(voltage < MIN_CONDUCTIVITY_VOLTAGE) {
        voltage = MIN_CONDUCTIVITY_VOLTAGE;
        printf("Limit exceeded!\n");
    }

    else if (voltage > MAX_CONDUCTIVITY_VOLTAGE) {
        voltage = MAX_CONDUCTIVITY_VOLTAGE;
        printf("Limit exceeded!\n");
    }

    /* temperature compensation (ref is 25C) */
    tc = 1.0 + 0.02*(temperature-25.0);

    vc = voltage / tc;

    cond = (133.42*vc*vc*vc - 255.86*vc*vc + 857.39*vc)*0.5; 

    return cond;
}
