#include <sys/time.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>

#include "app.h"

/* read from config */
#define MQTT_HOST_STR "robseago.mqtt.ubeac.io"
#define MQTT_PORT 1883
#define MQTT_CLIENT_ID "a88367b5-893e-455f-9337-94609f09f023"
#define MQTT_TOPIC "wq100"
#define DEVICE_ID "b827ebf4a93a"
#define MEASURE_INTERVAL_SECONDS 5 // 300 // 10 minutes ~100/day
#define MAX_LINELEN 32
#define ADC_SAMPLES 5

sensor_t Sensor;

static const char *channel_name[MAX_CHANNELS] = {"Turbidity", "TDS Conductivity", "pH", "Vref"};
static uint16_t adc_value[MAX_CHANNELS];
static double voltage[MAX_CHANNELS];

static char jstr[512];
static char cmd[1024];

static char WlanAddr[32];

/* The quick hacky way to do publish to mqtt */
void publish(void)
{
    sprintf(cmd, "mosquitto_pub -h %s -p %u -i %s -t %s -m '%s'\n",
	MQTT_HOST_STR,
	MQTT_PORT,
	MQTT_CLIENT_ID,
	MQTT_TOPIC,
	jstr);

printf("cmd:%s\n", cmd);
    system(cmd);
}

/* format to what ubeac is expecting */
void jsonize(void)
{
    sprintf(jstr, "{\"id\":\"%s\",\"sensors\":[\
        {\"id\":\"Temperature\",\"data\":%4.3f},\
        {\"id\":\"Voltage\",\"data\":%4.3f},\
        {\"id\":\"Turbidity\",\"data\":%4.3f},\
        {\"id\":\"Conductivity\",\"data\":%4.3f},\
        {\"id\":\"pH\",\"data\":%4.3f}\
        ]}",
	DEVICE_ID,
	Sensor.temperature,
	Sensor.voltage,
	Sensor.turbidity,
	Sensor.conductivity,
	Sensor.ph);
}

/* remove colons and newline chars */
static void strip_string(char *si, char *so)
{
    int i = 0;
    int o = 0;

    while(*(si+i) != 0x00) {
        if(isalnum(*(si+i))) {
            *(so+o) = *(si+i);
            o++;
        }
        i++;
    }
    *(so+o) = 0x00;
}

static void get_wlan_addr(void)
{
    FILE *fp;
    char fname[128];
    char si[MAX_LINELEN];

    sprintf(fname, "/sys/class/net/wlan0/address");
    fp = fopen(fname, "r");
    if(!fp) {
        printf("Failed to open %s!\n", fname);
        return;
    }

    /* expect a single line */
    if (fgets(si, sizeof(si), fp)) {
        strip_string(si, WlanAddr);
        printf("wlan mac: %s\n", WlanAddr);
    }
    else {
        printf("Failed to read wlan0 address!\n");
    }
    fclose(fp);
}

static void enable_sensor(int channel)
{
    uint8_t gid = gpio_lookup(channel);
    if(gid == 0xff) return;
    gpio_set_direction(gid, "out");
    gpio_set_value(gid, "1");
}

static void disable_sensor(int channel)
{
    uint8_t gid = gpio_lookup(channel);
    if(gid == 0xff) return;
    gpio_set_value(gid, "0");
    gpio_set_direction(gid, "in");
}

static void msleep(uint32_t ms)
{
    struct timespec req, rem;
    int rv;

    req.tv_sec = (time_t) (ms / 1000);
    long int ns = (long int)(ms) - req.tv_sec*1000; 
    req.tv_nsec = ns * 1000000;
    
    rv = nanosleep(&req, &rem);
    if(rv != 0) {
        printf("%s returned %d\n", __FUNCTION__, rv);
    }
}

static void cleanup_on_exit(void)
{
    printf("%s:", __FUNCTION__);
    i2c_release();
    printf(" released i2c,");
    gpio_unexport_all();
    printf(" unexported gpios,");
    printf(" done.\n");
}

void signalHandler(int sig)
{
    printf("Exiting...\n");
    exit(0);
}

typedef struct {
    int value;
    int valid;
}sample_t;

/* find the median of 5 samples */
static int sample_channel(int channel)
{
    sample_t s[ADC_SAMPLES];
    int mini, maxi;
    int minv, maxv;
    int i;

    mini = maxi = 0; // init to something
    minv = 100000;
    maxv = -100000;
    for(i = 0; i < ADC_SAMPLES; i++) {
        msleep(10); 
        s[i].value = ads1115_read(channel);
        s[i].valid = 1;

        if(s[i].value > maxv) {
            maxv = s[i].value;
            maxi = i;
        }
        if(s[i].value < minv) {
            minv = s[i].value;
            mini = i;
        }
    }

    /* discard hi and low */
    s[mini].valid = 0;
    s[maxi].valid = 0;

    minv = 100000;
    maxv = -100000;
    for(i = 0; i < ADC_SAMPLES; i++) {

        if(!s[i].valid) continue;

        if(s[i].value > maxv) {
            maxv = s[i].value;
            maxi = i;
        }
        if(s[i].value < minv) {
            minv = s[i].value;
            mini = i;
        }
    }

    /* discard hi and low */
    s[mini].valid = 0;
    s[maxi].valid = 0;

    int value = s[0].value;  // init to something
    for(i = 0; i < ADC_SAMPLES; i++) {

        if(s[i].valid) {
            value = s[i].value;
            break;
        }
    }
    return value;
}


int main(void)
{
    struct timeval tv0, tv1;
    int ds, du;
    unsigned int us, uu;
    int channel;

    signal(SIGINT, signalHandler);
    atexit(cleanup_on_exit);

    get_wlan_addr();

    /* Initialize GPIO */
    gpio_export_all();

    /* Initialize I2C bus */
    i2c_init();
    ds18b20_init(); 
    ads1115_init();

    while (1) {

        gettimeofday(&tv0, NULL);

        /* read the temperature using the 1-wire interface */
        ds18b20_read();
        printf("Temperature: %1.4fV\n", Sensor.temperature);

        /* read the 5V reference voltage on A3 */
        channel = 3;
        adc_value[channel] = sample_channel(channel);
        voltage[channel] = ads1115_convert_to_voltage(adc_value[channel]);
        Sensor.voltage = voltage[channel];
        printf("%s: %1.4fV  (%d)\n", channel_name[channel], Sensor.voltage, adc_value[channel]);


        /* read turbidity on A0 - 5V always on */
        channel = 0;
        adc_value[channel] = sample_channel(channel);
        voltage[channel] = ads1115_convert_to_voltage(adc_value[channel]);
        Sensor.turbidity = compute_turbidity(voltage[channel], Sensor.temperature);
        printf("%s: %1.4fNTU  [%1.4fV (%d)]\n", channel_name[channel], Sensor.turbidity, voltage[channel], adc_value[channel]);


        /* read TDS conductivity on A1 - 3V3 powered GPIO */
        channel = 1;
        enable_sensor(channel);
        msleep(2000); // settling time, 1s is not enough
        adc_value[channel] = sample_channel(channel);
        disable_sensor(channel);
        voltage[channel] = ads1115_convert_to_voltage(adc_value[channel]);
        Sensor.conductivity = compute_conductivity(voltage[channel], Sensor.temperature);
        printf("%s: %1.4f ppm  [%1.4fV (%d)]\n", channel_name[channel], Sensor.conductivity, voltage[channel], adc_value[channel]);

        msleep(1000); // does conductivity need to discharge after powering off


        /* read pH on A2 - 3V3 powered GPIO */
        channel = 2;
        enable_sensor(channel);
        msleep(1000); // important
        adc_value[channel] = sample_channel(channel);
        disable_sensor(channel);
        voltage[channel] = ads1115_convert_to_voltage(adc_value[channel]);
        Sensor.ph = compute_ph(voltage[channel], Sensor.temperature);
        printf("%s: %1.4f  [%1.4fV (%d)]\n", channel_name[channel], Sensor.ph, voltage[channel], adc_value[channel]);



        jsonize();
        publish();
        gettimeofday(&tv1, NULL);

        ds = MEASURE_INTERVAL_SECONDS - (tv1.tv_sec - tv0.tv_sec);
        us = (unsigned int)((ds > 0) ? ds : MEASURE_INTERVAL_SECONDS);
        
        du = tv1.tv_usec - tv0.tv_usec;
        if(du != 0) {
            uu = (unsigned int)((du < 0) ? du + 1000000 : du);
            usleep(uu);
        }
        sleep(us);
    }

    return 0;  // will call function registered by atexit()
}
