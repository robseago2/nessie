#include "app.h"

#define SLAVE_ADDR 0x48


#define FS_BIT15 32768.0
//#define FS_VOLTAGE 6.144
//#define PGA_SETTING 0b000
#define FS_VOLTAGE 4.096
#define PGA_SETTING 0b001

/* these are not portable - only little endian */

typedef union {
  struct {
    uint8_t mode : 1; 
    uint8_t pga : 3;
    uint8_t mux : 3;
    uint8_t os : 1; 
  }b;
  uint8_t reg;
}config_reg_msb;

typedef union {
  struct {
    uint8_t comp : 5;
    uint8_t dr : 3;
  }b;
  uint8_t reg;
}config_reg_lsb;

void ads1115_init(void)
{
}


int ads1115_read(int chan)
{
    uint8_t wbuf[3];
    uint8_t rbuf[2];
    int16_t val;	
    config_reg_msb crm;
    config_reg_lsb crl;
    int rv;

    crm.b.os = 1; // begin a single conversion
    switch(chan) {
        case 0: crm.b.mux = 0b100; break; // A0
        case 1: crm.b.mux = 0b101; break; // A1
        case 2: crm.b.mux = 0b110; break; // A2
        case 3: crm.b.mux = 0b111; break; // A3
    }
    crm.b.pga = PGA_SETTING;
    crm.b.mode = 1;  // single shot mode

    crl.b.dr = 0b100; // 128SPS 
    crl.b.comp = 0b00011; // disable comparator;

    // These three bytes are written to the ADS1115 to set the config register and start a conversion 
    wbuf[0] = 1;  // sets the pointer register so that the following two bytes write to the config register  
    wbuf[1] = crm.reg; // config register MSB
    wbuf[2] = crl.reg; // config register LSB

    rv = i2c_write(SLAVE_ADDR, wbuf, 3);
    if(rv != 0) {
        printf("i2c write error\n");
        return -1;
    }

    rbuf[0] = 0x00;
    rbuf[1] = 0x00;

/* TODO: timeout */
    // Wait for the conversion to complete, this requires bit 15 to change from 0->1
    while ((rbuf[0] & 0x80) == 0)	// readBuf[0] contains 8 MSBs of config register, AND with 10000000 to select bit 15
    {
        rv = i2c_read(SLAVE_ADDR, rbuf, 2);	// Read the config register into readBuf
        if(rv != 0) {
            printf("i2c_read failed\n");
            return -1;
        }
    }

    wbuf[0] = 0;					// Set pointer register to 0 to read from the conversion register
    i2c_write(SLAVE_ADDR, wbuf, 1);
  
    i2c_read(SLAVE_ADDR, rbuf, 2);		// Read the contents of the conversion register into readBuf

    val = (rbuf[0] << 8) | rbuf[1];	// Combine the two bytes of readBuf into a single 16 bit result 

//    printf("raw analog %04x (%d)\n", val, val);

    /* in our case we know V can't be negative, clip to zero */
    if(val < 0)
        val = 0;

    return val;
}

double ads1115_convert_to_voltage(int val)
{
    double voltage;

    if(val < 0) {
        printf("ADC value (%d) is invalid!\n", val);
        return 0.0;
    }

    voltage = ((double)val*FS_VOLTAGE)/FS_BIT15;
    return voltage; 
}