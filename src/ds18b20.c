#include <dirent.h>

#include "app.h"

/* 1 wire */
#define W1_DEVICES "/sys/bus/w1/devices"
#define DS_ID "28-"

static char Ds18b20[256];

int ds18b20_init(void)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(W1_DEVICES);
    if(!d) {
        printf("Invalid directory! [%s]\n", W1_DEVICES);
        return -1;
    }

    while ((dir = readdir(d)) != NULL) {
        if(strncmp(DS_ID, dir->d_name, 3) == 0) {
            snprintf(Ds18b20, sizeof(Ds18b20), "%s", dir->d_name);
            printf("Found ds18b20 at %s/%s\n", W1_DEVICES, Ds18b20);
        }
    }
    closedir(d);
    return(1);
}

void ds18b20_read(void)
{
    FILE *fp;
    int t;
    int rv;
    char fname[512];

    Sensor.temperature_valid = 0;
    sprintf(fname, "%s/%s/temperature", W1_DEVICES, Ds18b20);
    fp = fopen(fname, "r");
    if(!fp)  {
        return;
    }
    rv = fscanf(fp, "%d", &t);
    if(rv == 1) {
        Sensor.temperature_valid = 1;
        Sensor.temperature_raw = t;
        Sensor.temperature = t / 1000.0;
    }
    fclose(fp);
}

